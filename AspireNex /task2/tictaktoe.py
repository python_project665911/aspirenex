def print_board(board):
    print()
    for row in board:
        print(" | ".join(row))
        print("-" * 5)
    print()

def check_winner(board, player):
    # Check rows, columns, and diagonals for a win
    for i in range(3):
        if all([spot == player for spot in board[i]]):
            return True
        if all([board[j][i] == player for j in range(3)]):
            return True
    if all([board[i][i] == player for i in range(3)]):
        return True
    if all([board[i][2-i] == player for i in range(3)]):
        return True
    return False

def get_empty_spots(board):
    return [(i, j) for i in range(3) for j in range(3) if board[i][j] == " "]

def minimax(board, depth, is_maximizing):
    if check_winner(board, "O"):
        return 1
    if check_winner(board, "X"):
        return -1
    if not get_empty_spots(board):
        return 0

    if is_maximizing:
        max_eval = float('-inf')
        for (i, j) in get_empty_spots(board):
            board[i][j] = "O"
            eval = minimax(board, depth + 1, False)
            board[i][j] = " "
            max_eval = max(max_eval, eval)
        return max_eval
    else:
        min_eval = float('inf')
        for (i, j) in get_empty_spots(board):
            board[i][j] = "X"
            eval = minimax(board, depth + 1, True)
            board[i][j] = " "
            min_eval = min(min_eval, eval)
        return min_eval

def best_move(board):
    best_score = float('-inf')
    move = None
    for (i, j) in get_empty_spots(board):
        board[i][j] = "O"
        score = minimax(board, 0, False)
        board[i][j] = " "
        if score > best_score:
            best_score = score
            move = (i, j)
    return move

def play_tic_tac_toe():
    board = [[" " for _ in range(3)] for _ in range(3)]
    print_board(board)
    
    while True:
        # Human move
        move = input("Enter your move (row and column separated by space): ")
        move = tuple(map(int, move.split()))
        if board[move[0]][move[1]] != " ":
            print("Invalid move! Try again.")
            continue
        board[move[0]][move[1]] = "X"
        print_board(board)
        if check_winner(board, "X"):
            print("You win!")
            break
        if not get_empty_spots(board):
            print("It's a tie!")
            break
        
        # AI move
        ai_move = best_move(board)
        if ai_move:
            board[ai_move[0]][ai_move[1]] = "O"
            print("AI move:")
            print_board(board)
            if check_winner(board, "O"):
                print("AI wins!")
                break
        if not get_empty_spots(board):
            print("It's a tie!")
            break

if __name__ == "__main__":
    play_tic_tac_toe()
