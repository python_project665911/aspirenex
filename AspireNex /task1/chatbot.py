def chatbot_response(user_input):
    user_input = user_input.lower()
    
    # Greeting responses
    if "hello" in user_input or "hi" in user_input:
        return "Hello! How can I help you today?"
    
    # Farewell responses
    elif "bye" in user_input or "goodbye" in user_input:
        return "Goodbye! Have a great day!"
    
    # Name query
    elif "your name" in user_input:
        return "I am a simple chatbot created to assist you."
    
    # How are you query
    elif "how are you" in user_input:
        return "I'm just a program, so I don't have feelings, but I'm here to help you!"
    
    # Time query
    elif "time" in user_input:
        from datetime import datetime
        now = datetime.now()
        return f"The current time is {now.strftime('%H:%M:%S')}"
    
    # Date query
    elif "date" in user_input:
        from datetime import date
        today = date.today()
        return f"Today's date is {today.strftime('%B %d, %Y')}"
    
    # Weather query (static response)
    elif "weather" in user_input:
        return "I can't check the weather right now, but you can try a weather website or app."
    
    # Thank you response
    elif "thank you" in user_input or "thanks" in user_input:
        return "You're welcome!"
    
    # Hobby query
    elif "hobbies" in user_input:
        return "I enjoy chatting with you and learning new things."
    
    # Favorite color query
    elif "favorite color" in user_input:
        return "I don't have eyes, but I hear blue is quite popular!"
    
    # Favorite food query
    elif "favorite food" in user_input:
        return "I don't eat, but I hear pizza is delicious!"
    
    # Location query
    elif "where are you" in user_input:
        return "I exist in the digital world, hosted on a server."
    
    # Purpose query
    elif "purpose" in user_input:
        return "My purpose is to assist and chat with you!"
    
    # Joke query
    elif "tell me a joke" in user_input:
        return "Why don't scientists trust atoms? Because they make up everything!"
    
    # Music preference query
    elif "favorite music" in user_input:
        return "I don't listen to music, but I can help you find some recommendations!"
    
    # Default response for unrecognized queries
    else:
        return "I'm sorry, I don't understand that. Can you please rephrase?"

def main():
    print("Chatbot: Hello! I am a chatbot. Type 'bye' to exit.")
    while True:
        user_input = input("You: ")
        if user_input.lower() in ["bye", "goodbye"]:
            print("Chatbot: Goodbye! Have a great day!")
            break
        response = chatbot_response(user_input)
        print(f"Chatbot: {response}")

if __name__ == "__main__":
    main()
